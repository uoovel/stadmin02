package net.codejava.contact.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import net.codejava.contact.model.Ecriteria;

//import net.codejava.contact.model.Criteria;

public class EcriteriaDAOImpl implements EcriteriaDAO{
	private JdbcTemplate jdbcTemplate;
	
	public EcriteriaDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public int save(Ecriteria c) {
		String sql = "INSERT INTO ecriteria (ecritname) VALUES (?)";
		return jdbcTemplate.update(sql, c.getEcritname());
		//return 0;
	}

	@Override
	public int update(Ecriteria c) {
		//System.out.println("Kontr505: SubjectDAOImpl/update");
		String sql = "UPDATE ecriteria SET ecritname=? WHERE ecriteria_id=?";
		//System.out.println("Kontr510: SubjectDAOImpl/update" + c);
		//System.out.println("Kontr515: SubjectDAOImpl/update" + c.getSubname());
		return jdbcTemplate.update(sql, c.getEcritname(), c.getId());
	}

	@Override
	public Ecriteria get(Integer id) {
		String sql = "SELECT * FROM ecriteria WHERE ecriteria_id=" + id;
		ResultSetExtractor<Ecriteria> extractor = new ResultSetExtractor<Ecriteria>() {

			@Override
			public Ecriteria extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					String ecritname = rs.getString("ecritname");				
					return new Ecriteria(id, ecritname);
				}
				return null;
			}
			
		};
		
		return jdbcTemplate.query(sql, extractor);
	}

	@Override
	public int delete(Integer id) {
		String sql = "DELETE FROM ecriteria WHERE ecriteria_id=" + id;
		return jdbcTemplate.update(sql);
	}

	@Override
	public List<Ecriteria> list() {
		String sql = "SELECT * FROM ecriteria";
		RowMapper<Ecriteria> rowMapper = new RowMapper<Ecriteria>() {

			@Override
			public Ecriteria mapRow(ResultSet rs, int rowNum) throws SQLException {
				Integer id = rs.getInt("ecriteria_id");
				String ecritname = rs.getString("ecritname");
				return new Ecriteria(id, ecritname);
			}
			
		};		
		return jdbcTemplate.query(sql, rowMapper);		
	}
}
