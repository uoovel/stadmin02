package net.codejava.contact.dao;

import java.util.List;

import net.codejava.contact.model.Student;

public interface StudentDAO {
	public int save(Student student);
	
	public int update(Student student);
	
	public Student get(Integer id);
	
	public int delete(Integer id);
	
	public List<Student> list();
}
