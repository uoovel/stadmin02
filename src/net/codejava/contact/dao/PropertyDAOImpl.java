package net.codejava.contact.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import net.codejava.contact.model.Property;

//import net.codejava.contact.model.StudentPack;

public class PropertyDAOImpl implements PropertyDAO{
	private JdbcTemplate jdbcTemplate;
	
	public PropertyDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public int save(Property c) {
		String sql = "INSERT INTO property (student_id, criteria_id,"
				+ " description) VALUES (?, ?, ?)";
		return jdbcTemplate.update(sql, c.getStudentid(), c.getCriteriaid(),
				c.getDescription());
		//return 0;
	}

	@Override
	public int update(Property c) {
		String sql = "UPDATE property SET student_id=?, criteria_id=?,"
				+ "description=? WHERE property_id=?";		
		return jdbcTemplate.update(sql, c.getStudentid(), c.getCriteriaid(),
				c.getDescription(), c.getId());
	}

	@Override
	public Property get(Integer id) {
		String sql = "SELECT * FROM property WHERE property_id=" + id;
		ResultSetExtractor<Property> extractor = new ResultSetExtractor<Property>() {

			@Override
			public Property extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					//System.out.println();
					Integer id = rs.getInt("property_id");
					Integer studentid = rs.getInt("student_id");					
					Integer criteria_id = rs.getInt("criteria_id");					
					String description = rs.getString("description");
					return new Property(id, studentid, criteria_id, 
							description);
				}
				return null;
			}
			
		};
		
		return jdbcTemplate.query(sql, extractor);
	}

	@Override
	public int delete(Integer id) {
		String sql = "DELETE FROM property WHERE property_id=" + id;
		return jdbcTemplate.update(sql);
	}

	@Override
	public List<Property> list() {
		String sql = "SELECT * FROM property";
		RowMapper<Property> rowMapper = new RowMapper<Property>() {

			@Override
			public Property mapRow(ResultSet rs, int rowNum) throws SQLException {
				Integer id = rs.getInt("property_id");
				Integer studentid = rs.getInt("student_id");
				Integer criteriaid = rs.getInt("criteria_id");
				String description = rs.getString("description");
				return new Property(id, studentid, criteriaid, description);
			}
			
		};		
		return jdbcTemplate.query(sql, rowMapper);		
	}
}
