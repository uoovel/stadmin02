package net.codejava.contact.dao;

import java.util.List;

import net.codejava.contact.model.Criteria;



public interface CriteriaDAO {
	public int save(Criteria criteria);
	
	public int update(Criteria criteria);
	
	public Criteria get(Integer id);
	
	public int delete(Integer id);
	
	public List<Criteria> list();
}
