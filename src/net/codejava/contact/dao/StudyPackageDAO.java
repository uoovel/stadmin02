package net.codejava.contact.dao;

import java.util.List;

import net.codejava.contact.model.StudyPackage;


public interface StudyPackageDAO {
	public int save(StudyPackage studypackage);
	
	public int update(StudyPackage studypackage);
	
	public StudyPackage get(Integer id);
	
	public int delete(Integer id);
	
	public List<StudyPackage> list();
}
