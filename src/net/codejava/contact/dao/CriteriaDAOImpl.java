package net.codejava.contact.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import net.codejava.contact.model.Criteria;



public class CriteriaDAOImpl implements CriteriaDAO{
	private JdbcTemplate jdbcTemplate;
	
	public CriteriaDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public int save(Criteria c) {
		String sql = "INSERT INTO criteria (critname) VALUES (?)";
		return jdbcTemplate.update(sql, c.getCritname());
		//return 0;
	}

	@Override
	public int update(Criteria c) {
		//System.out.println("Kontr505: SubjectDAOImpl/update");
		String sql = "UPDATE criteria SET critname=? WHERE criteria_id=?";
		//System.out.println("Kontr510: SubjectDAOImpl/update" + c);
		//System.out.println("Kontr515: SubjectDAOImpl/update" + c.getSubname());
		return jdbcTemplate.update(sql, c.getCritname(), c.getId());
	}

	@Override
	public Criteria get(Integer id) {
		String sql = "SELECT * FROM criteria WHERE criteria_id=" + id;
		ResultSetExtractor<Criteria> extractor = new ResultSetExtractor<Criteria>() {

			@Override
			public Criteria extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					String critname = rs.getString("critname");				
					return new Criteria(id, critname);
				}
				return null;
			}
			
		};
		
		return jdbcTemplate.query(sql, extractor);
	}

	@Override
	public int delete(Integer id) {
		String sql = "DELETE FROM criteria WHERE criteria_id=" + id;
		return jdbcTemplate.update(sql);
	}

	@Override
	public List<Criteria> list() {
		String sql = "SELECT * FROM criteria";
		RowMapper<Criteria> rowMapper = new RowMapper<Criteria>() {

			@Override
			public Criteria mapRow(ResultSet rs, int rowNum) throws SQLException {
				Integer id = rs.getInt("criteria_id");
				String critname = rs.getString("critname");
				return new Criteria(id, critname);
			}
			
		};		
		return jdbcTemplate.query(sql, rowMapper);		
	}
}
