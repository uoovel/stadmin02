package net.codejava.contact.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import net.codejava.contact.model.PackageType;


public class PackageTypeDAOImpl implements PackageTypeDAO{
	private JdbcTemplate jdbcTemplate;
	
	public PackageTypeDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public int save(PackageType c) {
		String sql = "INSERT INTO packagetype (packname) VALUES (?)";
		return jdbcTemplate.update(sql, c.getPackname());
		//return 0;
	}

	@Override
	public int update(PackageType c) {
		//System.out.println("Kontr505: SubjectDAOImpl/update");
		String sql = "UPDATE packagetype SET packname=? "
				+ "WHERE packagetype_id=?";
		//System.out.println("Kontr510: SubjectDAOImpl/update" + c);
		//System.out.println("Kontr515: SubjectDAOImpl/update" + c.getSubname());
		return jdbcTemplate.update(sql, c.getPackname(), c.getId());
	}

	@Override
	public PackageType get(Integer id) {
		String sql = "SELECT * FROM packagetype WHERE packagetype_id=" + id;
		ResultSetExtractor<PackageType> extractor = new ResultSetExtractor<PackageType>() {

			@Override
			public PackageType extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					String packname = rs.getString("packname");				
					return new PackageType(id, packname);
				}
				return null;
			}
			
		};
		
		return jdbcTemplate.query(sql, extractor);
	}

	@Override
	public int delete(Integer id) {
		String sql = "DELETE FROM packagetype WHERE packagetype_id=" + id;
		return jdbcTemplate.update(sql);
	}

	@Override
	public List<PackageType> list() {
		String sql = "SELECT * FROM packagetype";
		RowMapper<PackageType> rowMapper = new RowMapper<PackageType>() {

			@Override
			public PackageType mapRow(ResultSet rs, int rowNum) throws SQLException {
				Integer id = rs.getInt("packagetype_id");
				String packname = rs.getString("packname");
				return new PackageType(id, packname);
			}
			
		};		
		return jdbcTemplate.query(sql, rowMapper);		
	}
}
