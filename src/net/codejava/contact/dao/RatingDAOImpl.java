package net.codejava.contact.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import net.codejava.contact.model.Rating;

//import net.codejava.contact.model.Property;

//import net.codejava.contact.model.Criteria;

public class RatingDAOImpl implements RatingDAO{
	private JdbcTemplate jdbcTemplate;
	
	public RatingDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public int save(Rating c) {
		String sql = "INSERT INTO rating (student_id, ecriteria_id,"
				+ " score, teacher_id) VALUES (?, ?, ?, ?)";
		return jdbcTemplate.update(sql, c.getStudentid(), c.getEcriteriaid(),
				c.getScore(), 0);
		//return 0;
	}

	@Override
	public int update(Rating c) {
		String sql = "UPDATE rating SET student_id=?, ecriteria_id=?,"
				+ "score=? WHERE rating_id=?";		
		return jdbcTemplate.update(sql, c.getStudentid(), c.getEcriteriaid(),
				c.getScore(), c.getId());
	}

	@Override
	public Rating get(Integer id) {
		String sql = "SELECT * FROM rating WHERE rating_id=" + id;
		ResultSetExtractor<Rating> extractor = new ResultSetExtractor<Rating>() {

			@Override
			public Rating extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					//System.out.println();
					Integer id = rs.getInt("rating_id");
					Integer studentid = rs.getInt("student_id");					
					Integer ecriteria_id = rs.getInt("ecriteria_id");					
					double score = rs.getDouble("score");
					return new Rating(id, studentid, ecriteria_id, 
							score);
				}
				return null;
			}
			
		};
		
		return jdbcTemplate.query(sql, extractor);
	}

	@Override
	public int delete(Integer id) {
		String sql = "DELETE FROM rating WHERE rating_id=" + id; //järg
		return jdbcTemplate.update(sql);
	}

	@Override
	public List<Rating> list() {
		String sql = "SELECT * FROM rating";
		RowMapper<Rating> rowMapper = new RowMapper<Rating>() {

			@Override
			public Rating mapRow(ResultSet rs, int rowNum) throws SQLException {
				Integer id = rs.getInt("rating_id");
				Integer studentid = rs.getInt("student_id");
				Integer ecriteriaid = rs.getInt("ecriteria_id");
				double score = rs.getDouble("score");
				return new Rating(id, studentid, ecriteriaid, score);
			}
			
		};		
		return jdbcTemplate.query(sql, rowMapper);		
	}

}
