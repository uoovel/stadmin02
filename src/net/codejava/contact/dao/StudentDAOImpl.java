package net.codejava.contact.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import net.codejava.contact.model.Student;

//import net.codejava.contact.model.Contact;

public class StudentDAOImpl implements StudentDAO{
	private JdbcTemplate jdbcTemplate;
	
	public StudentDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public int save(Student c) {
		String sql = "INSERT INTO student (studname, email) VALUES (?, ?)";
		return jdbcTemplate.update(sql, c.getStudname(), c.getEmail());
		//return 0;
	}

	@Override
	public int update(Student c) {
		String sql = "UPDATE student SET studname=?, email=? WHERE student_id=?";
		return jdbcTemplate.update(sql, c.getStudname(), c.getEmail(), c.getId());
	}

	@Override
	public Student get(Integer id) {
		String sql = "SELECT * FROM student WHERE student_id=" + id;
		ResultSetExtractor<Student> extractor = new ResultSetExtractor<Student>() {

			@Override
			public Student extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					String studname = rs.getString("studname");
					String email = rs.getString("email");
					return new Student(id, studname, email);
				}
				return null;
			}
			
		};
		
		return jdbcTemplate.query(sql, extractor);
	}

	@Override
	public int delete(Integer id) {
		String sql = "DELETE FROM student WHERE student_id=" + id;
		return jdbcTemplate.update(sql);
	}

	@Override
	public List<Student> list() {
		//System.out.println("Control110: StudentDAO");
		String sql = "SELECT * FROM student";
		RowMapper<Student> rowMapper = new RowMapper<Student>() {

			@Override
			public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
				Integer id = rs.getInt("student_id");
				String studname = rs.getString("studname");
				String email = rs.getString("email");
				return new Student(id, studname, email);
			}
			
		};		
		return jdbcTemplate.query(sql, rowMapper);		
	}
}
