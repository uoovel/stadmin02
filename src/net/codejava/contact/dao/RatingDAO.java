package net.codejava.contact.dao;

import java.util.List;

import net.codejava.contact.model.Rating;

//import net.codejava.contact.model.Ecriteria;

//import net.codejava.contact.model.Criteria;

public interface RatingDAO {
	public int save(Rating rating);
	
	public int update(Rating rating);
	
	public Rating get(Integer id);
	
	public int delete(Integer id);
	
	public List<Rating> list();
}
