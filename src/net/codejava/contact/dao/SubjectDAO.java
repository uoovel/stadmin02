package net.codejava.contact.dao;

import java.util.List;

import net.codejava.contact.model.Subject;

public interface SubjectDAO {
	public int save(Subject subject);
	
	public int update(Subject subject);
	
	public Subject get(Integer id);
	
	public int delete(Integer id);
	
	public List<Subject> list();
}
