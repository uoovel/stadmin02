package net.codejava.contact.dao;

import java.util.List;

import net.codejava.contact.model.Ecriteria;

//import net.codejava.contact.model.Criteria;

public interface EcriteriaDAO {
	public int save(Ecriteria ecriteria);
	
	public int update(Ecriteria ecriteria);
	
	public Ecriteria get(Integer id);
	
	public int delete(Integer id);
	
	public List<Ecriteria> list();
}
