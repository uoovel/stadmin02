package net.codejava.contact.dao;

import java.util.List;

import net.codejava.contact.model.PackageType;
//import net.codejava.contact.model.Subject;

public interface PackageTypeDAO {
	public int save(PackageType packagetype);
	
	public int update(PackageType packagetype);
	
	public PackageType get(Integer id);
	
	public int delete(Integer id);
	
	public List<PackageType> list();
}
