package net.codejava.contact.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import net.codejava.contact.dao.ContactDAO;
import net.codejava.contact.dao.ContactDAOImpl;
import net.codejava.contact.dao.CriteriaDAO;
import net.codejava.contact.dao.CriteriaDAOImpl;
import net.codejava.contact.dao.EcriteriaDAO;
import net.codejava.contact.dao.EcriteriaDAOImpl;
import net.codejava.contact.dao.PackageTypeDAO;
import net.codejava.contact.dao.PackageTypeDAOImpl;
import net.codejava.contact.dao.PropertyDAO;
import net.codejava.contact.dao.PropertyDAOImpl;
import net.codejava.contact.dao.RatingDAO;
import net.codejava.contact.dao.RatingDAOImpl;
import net.codejava.contact.dao.StudentDAO;
import net.codejava.contact.dao.StudentDAOImpl;
import net.codejava.contact.dao.StudentPackDAO;
import net.codejava.contact.dao.StudentPackDAOImpl;
import net.codejava.contact.dao.StudyPackageDAO;
import net.codejava.contact.dao.StudyPackageDAOImpl;
import net.codejava.contact.dao.SubjectDAO;
import net.codejava.contact.dao.SubjectDAOImpl;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "net.codejava.contact")
public class SpringMvcConfig implements WebMvcConfigurer{
	@Bean
	public DataSource getDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/contactdb");
		dataSource.setUsername("administrator");		
		dataSource.setPassword("Aprill2015+Aprill2015");
		
		return dataSource;
		
	}
	@Bean
	public ViewResolver getViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		return resolver;
	}
	@Bean
	public ContactDAO getContactDAO() {
		return new ContactDAOImpl(getDataSource());
	}
	@Bean
	public SubjectDAO getSubjectDAO() {
		return new SubjectDAOImpl(getDataSource());
	}
	@Bean
	public PackageTypeDAO getPackageTypeDAO() {
		return new PackageTypeDAOImpl(getDataSource());
	}
	@Bean
	public StudyPackageDAO getStudyPackageDAO() {
		return new StudyPackageDAOImpl(getDataSource());
	}
	@Bean
	public StudentDAO getStudentDAO() {
		//System.out.println("Control050: Config");
		return new StudentDAOImpl(getDataSource());
	}
	@Bean
	public StudentPackDAO getStudentPackDAO() {
		return new StudentPackDAOImpl(getDataSource());
	}
	@Bean
	public CriteriaDAO getCriteriaDAO() {
		return new CriteriaDAOImpl(getDataSource());
	}
	@Bean
	public PropertyDAO getPropertyDAO() {
		return new PropertyDAOImpl(getDataSource());
	}
	@Bean
	public EcriteriaDAO getEcriteriaDAO() {
		return new EcriteriaDAOImpl(getDataSource());
	}
	@Bean
	public RatingDAO getRatingDAO() {
		return new RatingDAOImpl(getDataSource());
	}
	
}
