package net.codejava.contact.model;

public class RatingView {
	private Integer id;
	private String studname;
	private String ecritname;
	private double score;
	//private double price;
	//private boolean available;
	
	private String ratingname;
	
	public RatingView() {
		
	}

	public RatingView(Integer id, String studname, String ecritname,
			double score) {		
		
		this(studname, ecritname, score);
		this.id = id;
	}
	public RatingView(String studname, String ecritname,
			double score) {
		this.studname = studname;
		this.ecritname = ecritname;
		this.score = score;
		
		this.ratingname = studname + "; " + ecritname + "; " + score;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStudname() {		
		return studname;
	}
	public void setStudname(String studname) {
		this.studname = studname;
	}
	public String getEcritname() {		
		return ecritname;
	}
	public void setEcritname(String ecritname) {
		this.ecritname = ecritname;
	}
	public double getScore() {		
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}

	
	public String getRatingname() {		
		return ratingname;
	}
	public void setRatingname(String ratingname) {
		this.ratingname = ratingname;
	}
	
	
	@Override
	public String toString() {
		return "RatingView [id=" + id + ", "
				+ "studname=" + studname + ", "
				+ "ecritname=" + ecritname + ", "
				+ "score=" + score			
				+ "]";
	}
}
