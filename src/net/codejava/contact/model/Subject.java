package net.codejava.contact.model;

public class Subject {
	private Integer id;
	private String subname;

	
	public Subject() {
		
	}

	public Subject(Integer id, String subname) {		
		
		this(subname);
		this.id = id;
	}
	public Subject(String subname) {
		
		
		this.subname = subname;

	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSubname() {
		//System.out.println("Kontr520:Subject/getSubname");
		return subname;
	}
	public void setSubname(String subname) {
		this.subname = subname;
	}

	@Override
	public String toString() {
		return "Subject [id=" + id + ", subname=" + subname
				+ "]";
	}
}
