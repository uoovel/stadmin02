package net.codejava.contact.model;

import java.sql.Date;

public class StudentPackView {
	private Integer id;
	//private StudyPackage studypackage;
	private StudyPackageView studypackageview;
	private String studname;
	private Date begin;
	
	public StudentPackView() {
		
	}

	public StudentPackView(Integer id, StudyPackageView studypackageview, String studname,
			Date begin) {		
		
		this(studypackageview, studname, begin);
		this.id = id;
	}
	public StudentPackView(StudyPackageView studypackageview, String studname,
			Date begin) {
		this.studypackageview = studypackageview;
		this.studname = studname;
		this.begin = begin;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public StudyPackageView getStudypackageview() {		
		return studypackageview;
	}
	public void setStudypackageview(StudyPackageView studypackageview) {
		this.studypackageview = studypackageview;
	}
	public String getStudname() {		
		return studname;
	}
	public void setStudname(String studname) {
		this.studname = studname;
	}
	
	public Date getBegin() {		
		return begin;
	}
	public void setBegin(Date begin) {
		this.begin = begin;
	}

	@Override
	public String toString() {
		return "StudentPackView [id=" + id + ", "
				+ "studypackageview=" + studypackageview + ", "
				+ "studname=" + studname + ", "
				+ "begin=" + begin
				+ "]";
	}
}
