package net.codejava.contact.model;

public class PackageType {
	private Integer id;
	private String packname;

	
	public PackageType() {
		
	}

	public PackageType(Integer id, String packname) {		
		
		this(packname);
		this.id = id;
	}
	public PackageType(String packname) {
		
		
		this.packname = packname;

	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPackname() {
		//System.out.println("Kontr520:Subject/getSubname");
		return packname;
	}
	public void setPackname(String packname) {
		this.packname = packname;
	}

	@Override
	public String toString() {
		return "PackageType [id=" + id + ", packname=" + packname
				+ "]";
	}
}
