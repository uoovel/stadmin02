package net.codejava.contact.model;

public class Student {
	private Integer id;
	private String studname;
	private String email;

	
	public Student() {
		
	}

	public Student(Integer id, String studname, String email) {	
		
		this(studname, email);		
		this.id = id;
	}
	public Student(String studname, String email) {
		//System.out.println("Control115: model Student");
		this.studname = studname;
		this.email = email;

	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStudname() {
		return studname;
	}
	public void setStudname(String studname) {
		this.studname = studname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	@Override
	public String toString() {
		return "Student [id=" + id + ", studname=" + studname + ", email=" + email
				+ "]";
	}
}
