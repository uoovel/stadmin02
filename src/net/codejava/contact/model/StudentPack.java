package net.codejava.contact.model;

import java.sql.Date;

public class StudentPack {
	private Integer id;
	private Integer studypackageid;	
	private Integer studentid;
	private Date begin;

	
	public StudentPack() {
		
	}

	public StudentPack(Integer id, Integer studypackageid, Integer studentid,
			 Date begin) {		
		
		this(studypackageid, studentid, begin);
		this.id = id;
	}
	public StudentPack(Integer studypackageid, Integer studentid,
			Date begin) {
		this.studypackageid = studypackageid;
		this.studentid = studentid;
		this.begin = begin;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getStudypackageid() {		
		return studypackageid;
	}
	public void setStudypackageid(Integer studypackageid) {
		this.studypackageid = studypackageid;
	}
	public Integer getStudentid() {		
		return studentid;
	}
	public void setStudentid(Integer studentid) {
		this.studentid = studentid;
	}

	public Date getBegin() {		
		return begin;
	}
	public void setBegin(Date begin) {
		this.begin = begin;
	}

	@Override
	public String toString() {
		return "StudentPack [id=" + id + ", "
				+ "studypackageid=" + studypackageid + ", "
				+ "studentid=" + studentid + ", "
				+ "begin=" + begin
				+ "]";
	}
}
