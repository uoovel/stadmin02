package net.codejava.contact.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.CriteriaDAO;
import net.codejava.contact.model.Criteria;

//import net.codejava.contact.dao.SubjectDAO;
//import net.codejava.contact.model.Subject;

@Controller
public class CriteriaController {
	
	@Autowired
	private CriteriaDAO criteriaDAO;
	
	//Subjects
	@RequestMapping(value = "/administration/criterias")
	public ModelAndView listCriteria(ModelAndView model) {
		List<Criteria> listCriteria = criteriaDAO.list();
		model.addObject("listCriteria", listCriteria);
		model.setViewName("administration/criterias");		
		return model;
	}	
	@RequestMapping(value = "/administration/newcriteria", method = RequestMethod.GET)
	public ModelAndView newCriteria(ModelAndView model) {
		Criteria newCriteria = new Criteria();
		model.addObject("criteria", newCriteria);
		model.setViewName("administration/criteria_form");		
		return model;
		
	}
	@RequestMapping(value = "/administration/savecriteria", method = RequestMethod.POST)
	public ModelAndView saveCriteria(@ModelAttribute Criteria criteria){
		if (criteria.getId() == null) {
			criteriaDAO.save(criteria);
		}else {
			//System.out.println("Kont500: MainController" + subject);
			criteriaDAO.update(criteria);
			//System.out.println("Kont600: MainController");
		}		
		
		return new ModelAndView("redirect:/administration/criterias");
	}
	
	@RequestMapping(value = "/administration/editcriteria", method = RequestMethod.GET)
	public ModelAndView editCriteria(HttpServletRequest request){
		Integer id = Integer.parseInt(request.getParameter("id"));
		Criteria criteria = criteriaDAO.get(id);
		ModelAndView model = new ModelAndView("administration/criteria_form");		
		model.addObject("criteria", criteria);		
		return model;
	}
	
	@RequestMapping(value = "/administration/deletecriteria", method = RequestMethod.GET)
	public ModelAndView deleteCriteria(@RequestParam Integer id){
		criteriaDAO.delete(id);
		return new ModelAndView("redirect:/administration/criterias");
	}
}
