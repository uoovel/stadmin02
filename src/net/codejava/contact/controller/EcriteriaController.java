package net.codejava.contact.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.EcriteriaDAO;
import net.codejava.contact.model.Ecriteria;

//import net.codejava.contact.dao.CriteriaDAO;
//import net.codejava.contact.model.Criteria;

@Controller
public class EcriteriaController {
	
	@Autowired
	private EcriteriaDAO ecriteriaDAO;
	
	//Subjects
	@RequestMapping(value = "/administration/ecriterias")
	public ModelAndView listEcriteria(ModelAndView model) {
		List<Ecriteria> listEcriteria = ecriteriaDAO.list();
		model.addObject("listEcriteria", listEcriteria);
		model.setViewName("administration/ecriterias");		
		return model;
	}	
	@RequestMapping(value = "/administration/newecriteria", method = RequestMethod.GET)
	public ModelAndView newEcriteria(ModelAndView model) {
		Ecriteria newEcriteria = new Ecriteria();
		model.addObject("ecriteria", newEcriteria);
		model.setViewName("administration/ecriteria_form");		
		return model;
		
	}
	@RequestMapping(value = "/administration/saveecriteria", method = RequestMethod.POST)
	public ModelAndView saveEcriteria(@ModelAttribute Ecriteria ecriteria){
		if (ecriteria.getId() == null) {
			ecriteriaDAO.save(ecriteria);
		}else {
			//System.out.println("Kont500: MainController" + subject);
			ecriteriaDAO.update(ecriteria);
			//System.out.println("Kont600: MainController");
		}		
		
		return new ModelAndView("redirect:/administration/ecriterias");
	}
	
	@RequestMapping(value = "/administration/editecriteria", method = RequestMethod.GET)
	public ModelAndView editEcriteria(HttpServletRequest request){
		Integer id = Integer.parseInt(request.getParameter("id"));
		Ecriteria ecriteria = ecriteriaDAO.get(id);
		ModelAndView model = new ModelAndView("administration/ecriteria_form");		
		model.addObject("ecriteria", ecriteria);		
		return model;
	}
	
	@RequestMapping(value = "/administration/deleteecriteria", method = RequestMethod.GET)
	public ModelAndView deleteEcriteria(@RequestParam Integer id){
		ecriteriaDAO.delete(id);
		return new ModelAndView("redirect:/administration/ecriterias");
	}
}
