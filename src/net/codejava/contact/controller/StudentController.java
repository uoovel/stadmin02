package net.codejava.contact.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.ContactDAO;
import net.codejava.contact.dao.StudentDAO;
import net.codejava.contact.model.Contact;
import net.codejava.contact.model.Student;

@Controller
public class StudentController {
	@Autowired
	private StudentDAO studentDAO;			

	
	@RequestMapping(value = "/administration/students")
	public ModelAndView listStudent(ModelAndView model) {
		System.out.println("Control100: Controller");
		List<Student> listStudent = studentDAO.list();
		System.out.println("Control200: Controller");
		model.addObject("listStudent", listStudent);
		System.out.println("Control300: Controller");
		model.setViewName("/administration/students");	
		System.out.println("Control400: Controller");
		return model;
	}	
	
	@RequestMapping(value = "/administration/newstudent", method = RequestMethod.GET)
	public ModelAndView newStudent(ModelAndView model) {
		Student newStudent = new Student();
		model.addObject("student", newStudent);
		model.setViewName("/administration/student_form");		
		return model;
		
	}
	
	@RequestMapping(value = "/administration/savestudent", method = RequestMethod.POST)
	public ModelAndView saveStudent(@ModelAttribute Student student){
		if (student.getId() == null) {
			studentDAO.save(student);
		}else {
			//System.out.println("Kont200: MainController" + contact);
			studentDAO.update(student);
		}		
		return new ModelAndView("redirect:/administration/students");
	}
	
	@RequestMapping(value = "/administration/editstudent", method = RequestMethod.GET)
	public ModelAndView editStudent(HttpServletRequest request){
		Integer id = Integer.parseInt(request.getParameter("id"));
		Student student = studentDAO.get(id);
		ModelAndView model = new ModelAndView("/administration/student_form");		
		model.addObject("student", student);		
		return model;
	}
	
	@RequestMapping(value = "/administration/deletestudent", method = RequestMethod.GET)
	public ModelAndView deleteStudent(@RequestParam Integer id){
		studentDAO.delete(id);
		return new ModelAndView("redirect:/administration/students");
	}
}
