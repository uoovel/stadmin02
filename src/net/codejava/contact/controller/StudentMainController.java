package net.codejava.contact.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class StudentMainController {
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView listStudent(ModelAndView model) {
		//List<Student> listStudent = studentDAO.list();
		//model.addObject("listStudent", listStudent);
		model.setViewName("/index");		
		return model;
	}	
}
