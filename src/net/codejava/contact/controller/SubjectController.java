package net.codejava.contact.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.SubjectDAO;
import net.codejava.contact.model.Subject;

@Controller
public class SubjectController {
	@Autowired
	private SubjectDAO subjectDAO;
	
	//Subjects
	@RequestMapping(value = "/administration/subjects")
	public ModelAndView listSubject(ModelAndView model) {
		List<Subject> listSubject = subjectDAO.list();
		model.addObject("listSubject", listSubject);
		model.setViewName("administration/subjects");		
		return model;
	}	
	@RequestMapping(value = "/administration/newsubject", method = RequestMethod.GET)
	public ModelAndView newSubject(ModelAndView model) {
		Subject newSubject = new Subject();
		model.addObject("subject", newSubject);
		model.setViewName("administration/subject_form");		
		return model;
		
	}
	@RequestMapping(value = "/administration/savesubject", method = RequestMethod.POST)
	public ModelAndView saveSubject(@ModelAttribute Subject subject){
		if (subject.getId() == null) {
			subjectDAO.save(subject);
		}else {
			System.out.println("Kont500: MainController" + subject);
			subjectDAO.update(subject);
			System.out.println("Kont600: MainController");
		}		
		
		return new ModelAndView("redirect:/administration/subjects");
	}
	
	@RequestMapping(value = "/administration/editsubject", method = RequestMethod.GET)
	public ModelAndView editSubject(HttpServletRequest request){
		Integer id = Integer.parseInt(request.getParameter("id"));
		Subject subject = subjectDAO.get(id);
		ModelAndView model = new ModelAndView("administration/subject_form");		
		model.addObject("subject", subject);		
		return model;
	}
	
	@RequestMapping(value = "/administration/deletesubject", method = RequestMethod.GET)
	public ModelAndView deleteSubject(@RequestParam Integer id){
		subjectDAO.delete(id);
		return new ModelAndView("redirect:/administration/subjects");
	}
}
