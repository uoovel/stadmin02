package net.codejava.contact.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.ContactDAO;
import net.codejava.contact.dao.PackageTypeDAO;
import net.codejava.contact.dao.SubjectDAO;
import net.codejava.contact.model.Contact;
import net.codejava.contact.model.PackageType;
import net.codejava.contact.model.Subject;

@Controller
public class MainController {
		
	@Autowired
	private ContactDAO contactDAO;			

	@RequestMapping(value ="/administration/index")
	public ModelAndView frontPage(ModelAndView model) {
		model.setViewName("administration/index");
		return model;
	}	
	
	@RequestMapping(value = "/administration/teachers")
	public ModelAndView listContact(ModelAndView model) {
		List<Contact> listContact = contactDAO.list();
		model.addObject("listContact", listContact);
		model.setViewName("administration/teachers");		
		return model;
	}	
	
	@RequestMapping(value = "/administration/new", method = RequestMethod.GET)
	public ModelAndView newContact(ModelAndView model) {
		Contact newContact = new Contact();
		model.addObject("contact", newContact);
		model.setViewName("administration/contact_form");		
		return model;
		
	}
	
	@RequestMapping(value = "/administration/save", method = RequestMethod.POST)
	public ModelAndView saveContact(@ModelAttribute Contact contact){
		if (contact.getId() == null) {
			contactDAO.save(contact);
		}else {
			//System.out.println("Kont200: MainController" + contact);
			contactDAO.update(contact);
		}		
		return new ModelAndView("redirect:/administration/teachers");
	}
	
	@RequestMapping(value = "/administration/edit", method = RequestMethod.GET)
	public ModelAndView editContact(HttpServletRequest request){
		Integer id = Integer.parseInt(request.getParameter("id"));
		Contact contact = contactDAO.get(id);
		ModelAndView model = new ModelAndView("administration/contact_form");		
		model.addObject("contact", contact);		
		return model;
	}
	
	@RequestMapping(value = "/administration/delete", method = RequestMethod.GET)
	public ModelAndView deleteContact(@RequestParam Integer id){
		contactDAO.delete(id);
		return new ModelAndView("redirect:/administration/teachers");
	}

}
