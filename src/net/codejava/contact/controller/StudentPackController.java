package net.codejava.contact.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.ContactDAO;
import net.codejava.contact.dao.PackageTypeDAO;
import net.codejava.contact.dao.StudentDAO;
import net.codejava.contact.dao.StudentPackDAO;
import net.codejava.contact.dao.StudyPackageDAO;
import net.codejava.contact.dao.SubjectDAO;
import net.codejava.contact.model.Contact;
import net.codejava.contact.model.PackageType;
import net.codejava.contact.model.Student;
import net.codejava.contact.model.StudentPack;
import net.codejava.contact.model.StudentPackView;
import net.codejava.contact.model.StudyPackage;
import net.codejava.contact.model.StudyPackageView;
import net.codejava.contact.model.Subject;

//import net.codejava.contact.dao.ContactDAO;
//import net.codejava.contact.dao.PackageTypeDAO;
//import net.codejava.contact.dao.StudyPackageDAO;
//import net.codejava.contact.dao.SubjectDAO;
//import net.codejava.contact.model.Contact;
//import net.codejava.contact.model.PackageType;
//import net.codejava.contact.model.StudyPackage;
//import net.codejava.contact.model.StudyPackageView;
//import net.codejava.contact.model.Subject;

@Controller
public class StudentPackController {
	@Autowired
	private StudentPackDAO studentpackDAO;
	
	@Autowired
	private StudyPackageDAO studypackageDAO;
	
	@Autowired
	private StudentDAO studentDAO;	
	
	@Autowired
	private SubjectDAO subjectDAO;
	
	@Autowired
	private PackageTypeDAO packagetypeDAO;	
	
	@Autowired
	private ContactDAO contactDAO;	

	//Järg
	//PackageTypes
	@RequestMapping(value = "/administration/studentpacks")
	public ModelAndView listStudentPack(ModelAndView model) {
		List<StudentPack> listStudentPack = studentpackDAO.list();
		//System.out.println("Kont500: StudyPackageController: " + listStudyPackage);
		model.addObject("listStudentPack", listStudentPack); //the left argument is correspondent to field in .jsp
		//////////
		List<StudentPackView> listStudentPackView = new ArrayList<>();
		
		for (int i = 0; i < listStudentPack.size(); i++){		
			
			StudentPack studentpack = listStudentPack.get(i);
			
			Integer id = studentpack.getId();//*
			
			//studypackageview plokk
			Integer studypackageid = studentpack.getStudypackageid();//*						
			StudyPackage studypackage = studypackageDAO.get(studypackageid);
			
			Integer subjectid = studypackage.getSubjectid();
			Subject subject = subjectDAO.get(subjectid);
			String subname = subject.getSubname();//*	
			
			Integer packagetypeid = studypackage.getPackagetypeid();
			PackageType packagetype = packagetypeDAO.get(packagetypeid);
			String packname = packagetype.getPackname();//*			

			Integer teacherid = studypackage.getTeacherid();
			Contact teacher = contactDAO.get(teacherid);
			String name = teacher.getName();//*				

			Double price = studypackage.getPrice();//*	
			
			Boolean available = studypackage.getAvailable();
			
			StudyPackageView studypackageview = new StudyPackageView(studypackageid, subname, packname, name, price, available);
            //
			
			Integer studentid = studentpack.getStudentid();						
			Student student = studentDAO.get(studentid);
			String studname = student.getStudname();//*
			
			Date begin = studentpack.getBegin();
			
			
			StudentPackView studentpackview = new StudentPackView(id, studypackageview, studname, begin);
			listStudentPackView.add(studentpackview);
			
		};
		
		//System.out.println(listStudentPackView);
		
		
		model.addObject("listStudentPackView", listStudentPackView);
		
		
		
		
		///////////
		model.setViewName("/administration/studentpacks");
		
		return model;
	}	
	@RequestMapping(value = "/administration/newstudentpack", method = RequestMethod.GET)
	public ModelAndView newStudentPack(ModelAndView model) {
		//List<Integer> listSubject = new ArrayList<Integer>();//my
		//listSubject.add(2);//my
		//System.out.println("StudentPackController500:");
		List<StudyPackage> listStudypackage = studypackageDAO.list(); //my	
		//System.out.println("StudentPackController600:" + listStudypackage);
		model.addObject("listStudypackage", listStudypackage);//my
		///////////////////////////////////
		//järg
		
		List<StudyPackageView> listStudyPackageView = new ArrayList<>();
		
		for (int i = 0; i < listStudypackage.size(); i++){		
			
			StudyPackage studypackage = listStudypackage.get(i);
			
			Integer id = studypackage.getId();//*
			
			Integer subjectid = studypackage.getSubjectid();						
			Subject subject = subjectDAO.get(subjectid);
			String subname = subject.getSubname();//*
			
			Integer packagetypeid = studypackage.getPackagetypeid();						
			PackageType packagetype = packagetypeDAO.get(packagetypeid);
			String packname = packagetype.getPackname();//*
			
			Integer teacherid = studypackage.getTeacherid();						
			Contact teacher = contactDAO.get(teacherid);
			String name = teacher.getName();//*
			
			double price = studypackage.getPrice();
			boolean available = studypackage.getAvailable();
			
			StudyPackageView studypackageview = new StudyPackageView(id, subname, packname, name, price, available);
			listStudyPackageView.add(studypackageview);
			
		};
		
		//System.out.println(listStudyPackageView);
		
		
		model.addObject("listStudyPackageView", listStudyPackageView);
	    
		
		
		
		
		
		/////////////////////////////////
		List<Student> listStudent = studentDAO.list(); //my		
		model.addObject("listStudent", listStudent);//my			
		
		
		StudentPack newStudentPack = new StudentPack();
		model.addObject("studentpack", newStudentPack);
		
		
		model.setViewName("/administration/studentpack_form");	
		//System.out.println("Kont600: StudyPackageController /newstudypackage: ");
		return model;
		
	}
	@RequestMapping(value = "/administration/savestudentpack", method = RequestMethod.POST)
	public ModelAndView saveStudentPack(@ModelAttribute StudentPack studentpack){
		//System.out.println("Kont700: StudyPackageController" + studypackage);
		if (studentpack.getId() == null) {
			studentpackDAO.save(studentpack);
		}else {
			
			studentpackDAO.update(studentpack);
			
		}		
		
		return new ModelAndView("redirect:/administration/studentpacks");
	}
	
	@RequestMapping(value = "/administration/editstudentpack", method = RequestMethod.GET)
	public ModelAndView editStudentPack(HttpServletRequest request){
		
		Integer id = Integer.parseInt(request.getParameter("id"));		
		
		StudentPack studentpack = studentpackDAO.get(id);
		//System.out.println("Kont800: MainController: " + request.getParameter("id"));
		//System.out.println("Kont805: MainController: " + studypackage);
		
		ModelAndView model = new ModelAndView("/administration/studentpack_edit");	
		
		
		model.addObject("studentpack", studentpack);
		
		
		Integer studypackageid = studentpack.getStudypackageid(); //my
		//System.out.println("Kont810: MainController: " + subjectid);
		StudyPackage studypackage = studypackageDAO.get(studypackageid); //my		
		model.addObject("studypackage", studypackage);//my		
		List<StudyPackage> listStudypackage = studypackageDAO.list(); //my		
		model.addObject("listStudypackage", listStudypackage);//my
		
		//studypackageview
		Integer subjectid = studypackage.getSubjectid();						
		Subject subject = subjectDAO.get(subjectid);
		String subname = subject.getSubname();//*
		
		Integer packagetypeid = studypackage.getPackagetypeid();						
		PackageType packagetype = packagetypeDAO.get(packagetypeid);
		String packname = packagetype.getPackname();//*
		
		Integer teacherid = studypackage.getTeacherid();						
		Contact teacher = contactDAO.get(teacherid);
		String name = teacher.getName();//*
		
		double price = studypackage.getPrice();
		boolean available = studypackage.getAvailable();
        StudyPackageView studypackageview = new StudyPackageView(studypackageid, subname, packname, name, price, available);
        model.addObject("studypackageview", studypackageview);
		
		//////////liststudypackageview
		List<StudyPackageView> listStudyPackageView = new ArrayList<>();
		
		for (int i = 0; i < listStudypackage.size(); i++){		
			
			studypackage = listStudypackage.get(i);
			
			id = studypackage.getId();//*
			
			subjectid = studypackage.getSubjectid();						
			subject = subjectDAO.get(subjectid);
			subname = subject.getSubname();//*
			
			packagetypeid = studypackage.getPackagetypeid();						
			packagetype = packagetypeDAO.get(packagetypeid);
			packname = packagetype.getPackname();//*
			
			teacherid = studypackage.getTeacherid();						
			teacher = contactDAO.get(teacherid);
			name = teacher.getName();//*
			
			price = studypackage.getPrice();
			available = studypackage.getAvailable();
			
			studypackageview = new StudyPackageView(id, subname, packname, name, price, available);
			listStudyPackageView.add(studypackageview);
			
		};
		
		//System.out.println(listStudyPackageView);
		
		
		model.addObject("listStudyPackageView", listStudyPackageView);
		
		
		
		
		///////////		
		
		
		
		
		
		
		Integer studentid = studentpack.getStudentid(); //my		
		Student student = studentDAO.get(studentid); //my		
		model.addObject("student", student);//my		
		List<Student> listStudent = studentDAO.list(); //my		
		model.addObject("listStudent", listStudent);//my		
	
		
		
		//System.out.println("Kont815: MainController: " + subject);
		
		return model;
	}
	
	@RequestMapping(value = "/administration/deletestudentpack", method = RequestMethod.GET)
	public ModelAndView deleteStudentPack(@RequestParam Integer id){
		studentpackDAO.delete(id);
		return new ModelAndView("redirect:/administration/studentpacks");
	}
}
