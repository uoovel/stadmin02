package net.codejava.contact.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.PackageTypeDAO;
import net.codejava.contact.model.PackageType;



@Controller
public class PackageTypeController {
	@Autowired
	private PackageTypeDAO packagetypeDAO;
	//PackageTypes
	@RequestMapping(value = "/administration/packagetypes")
	public ModelAndView listPackageTypes(ModelAndView model) {
		List<PackageType> listPackageType = packagetypeDAO.list();
		model.addObject("listPackageType", listPackageType);
		model.setViewName("administration/packagetypes");		
		return model;
	}	
	@RequestMapping(value = "/administration/newpackagetype", method = RequestMethod.GET)
	public ModelAndView newPackageType(ModelAndView model) {
		PackageType newPackageType = new PackageType();
		model.addObject("packagetype", newPackageType);
		model.setViewName("administration/packagetype_form");		
		return model;
		
	}
	@RequestMapping(value = "/administration/savepackagetype", method = RequestMethod.POST)
	public ModelAndView savePackageType(@ModelAttribute PackageType packagetype){
		if (packagetype.getId() == null) {
			packagetypeDAO.save(packagetype);
		}else {
			//System.out.println("Kont500: MainController" + subject);
			packagetypeDAO.update(packagetype);
			//System.out.println("Kont600: MainController");
		}		
		
		return new ModelAndView("redirect:/administration/packagetypes");
	}
	
	@RequestMapping(value = "/administration/editpackagetype", method = RequestMethod.GET)
	public ModelAndView editPackageType(HttpServletRequest request){
		Integer id = Integer.parseInt(request.getParameter("id"));
		PackageType packagetype = packagetypeDAO.get(id);
		ModelAndView model = new ModelAndView("administration/packagetype_form");		
		model.addObject("packagetype", packagetype);		
		return model;
	}
	
	@RequestMapping(value = "/administration/deletepackagetype", method = RequestMethod.GET)
	public ModelAndView deletePackageType(@RequestParam Integer id){
		packagetypeDAO.delete(id);
		return new ModelAndView("redirect:/administration/packagetypes");
	}
	

}
