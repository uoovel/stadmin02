package net.codejava.contact.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.model.Student;

@Controller
public class StudentOnlyController {
	
	@RequestMapping(value = "/studentview/studentonly", method = RequestMethod.GET)
	public ModelAndView listStudent(ModelAndView model) {
		//List<Student> listStudent = studentDAO.list();
		//model.addObject("listStudent", listStudent);
		model.addObject("title", "Student only page");
		model.addObject("message", "Personal data of single student");
		model.setViewName("studentview/studentonly");
		
		return model;
	}	
}
