<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>PackageType Manager Home</title>
</head>
<body>
	<div align="center">
		<a href="index">Home</a>
		<h1>PackageType List</h1>
		<h3><a href="newpackagetype">New PackageType</a> </h3>
		<table border="1" cellpadding="5">
			<tr>
				<th>No</th>
				<th>Package Type Name</th>
			</tr>
			<c:forEach items="${listPackageType}" var="packagetype" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				<td>${packagetype.packname}</td>
				<td>
					<a href="editpackagetype?id=${packagetype.id }">Edit</a>
					&nbsp;&nbsp;
					<a href="deletepackagetype?id=${packagetype.id }">Delete</a>
				</td>
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>