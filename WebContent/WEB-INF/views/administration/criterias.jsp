<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Criteria Manager Home</title>
</head>
<body>
	<div align="center">
		<a href="index">Home</a>
		<h1>Criteria List</h1>
		<h3><a href="newcriteria">New Criteria</a> </h3>
		<table border="1" cellpadding="5">
			<tr>
				<th>No</th>
				<th>Criteria Name</th>
			</tr>
			<c:forEach items="${listCriteria}" var="criteria" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				<td>${criteria.critname}</td>
				<td>
					<a href="editcriteria?id=${criteria.id }">Edit</a>
					&nbsp;&nbsp;
					<a href="deletecriteria?id=${criteria.id }">Delete</a>
				</td>
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>