<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>New/Edit Property</title>
</head>
<body>
	<div align="center">
		<h1>New/Edit Property</h1>
		<form:form action="saveproperty" method="post" modelAttribute="property">
		<table cellpadding="$">
			<form:hidden path="id"/>
			<tr>
				<td>StudentId:</td>

				<td>
					<form:select path="studentid">						
						<form:option value="${student.id}" label="${student.studname}"/>
						<form:options items="${listStudent}" 
						itemValue="id" itemLabel="studname"/>	
					</form:select>
				</td>
			</tr>
			<tr>
				<td>CriteriaId:</td>
				<td>
					<form:select path="criteriaid">						
						<form:option value="${criteria.id}" label="${criteria.critname}"/>
						<form:options items="${listCriteria}" 
						itemValue="id" itemLabel="critname"/>	
					</form:select>
				</td>				
				
			</tr>

			<tr>
				<td>Description:</td>
				<td><form:input path="description"/></td>
			</tr>

			<tr>
				<td colspan="2" align="center"><input type="submit" value="SaveProperty"/></td>
			
			</tr>
		</table>		
		</form:form>
	
	</div>

</body>
</html>