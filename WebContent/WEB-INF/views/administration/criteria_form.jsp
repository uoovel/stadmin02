<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>New/Edit Criteria</title>
</head>
<body>
	<div align="center">
		<h1>New/Edit Criteria</h1>
		<form:form action="savecriteria" method="post" modelAttribute="criteria">
		<table cellpadding="$">
			<form:hidden path="id"/>
			<tr>
				<td>Criteria Name:</td>
				<td><form:input path="critname"/></td>
			</tr>

			<tr>
				<td colspan="2" align="center"><input type="submit" value="SaveCriteria"/></td>
			
			</tr>
		</table>		
		</form:form>
	
	</div>

</body>

</html>