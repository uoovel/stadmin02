<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Contact Manager Home</title>
</head>
<body>
	<div align="center">
		<a href="index">Home</a>
		<h1>StudyPackages List</h1>
		<h3><a href="newstudypackage">New StudyPackage</a> </h3>
		<table border="1" cellpadding="5">
			<tr>
				<th>No</th>
				<th>Subject</th>
				<th>Package Type</th>
				<th>Teacher</th>
				<th>Price</th>
				<th>Available</th>
				<th>Action</th>
			</tr>
			<c:forEach items="${listStudyPackageView}" var="studypackageview" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				
				<td>${studypackageview.subname}</td>
				<td>${studypackageview.packname}</td>
				<td>${studypackageview.name}</td>
				<td>${studypackageview.price}</td>
				<td>${studypackageview.available}</td>
				<td>
					<a href="editstudypackage?id=${studypackageview.id }">Edit</a>
					&nbsp;&nbsp;
					<a href="deletestudypackage?id=${studypackageview.id }">Delete</a>
				</td>
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>