<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ecriteria Manager Home</title>
</head>
<body>
	<div align="center">
		<a href="index">Home</a>
		<h1>Ecriteria List (evaluation)</h1>
		<h3><a href="newecriteria">New Ecriteria</a> </h3>
		<table border="1" cellpadding="5">
			<tr>
				<th>No</th>
				<th>Ecriteria Name</th>
			</tr>
			<c:forEach items="${listEcriteria}" var="ecriteria" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				<td>${ecriteria.ecritname}</td>
				<td>
					<a href="editecriteria?id=${ecriteria.id }">Edit</a>
					&nbsp;&nbsp;
					<a href="deleteecriteria?id=${ecriteria.id }">Delete</a>
				</td>
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>