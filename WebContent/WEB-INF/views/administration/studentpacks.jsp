<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Students In Packs</title>
</head>
<body>
	<div align="center">
		<a href="index">Home</a>
		<h1>Registered students</h1>
		<h3><a href="newstudentpack">Registration of New Student</a> </h3>
		<table border="1" cellpadding="5">
			<tr>
				<th>No</th>
				<th>Study Package</th>			
				<th>Student</th>
				<th>Begin</th>
			</tr>
			<c:forEach items="${listStudentPackView}" var="studentpackview" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				
				<td>${studentpackview.studypackageview}</td>
				<td>${studentpackview.studname}</td>
				<td>${studentpackview.begin}</td>
				<td>
					<a href="editstudentpack?id=${studentpackview.id }">Edit</a>
					&nbsp;&nbsp;
					<a href="deletestudentpack?id=${studentpackview.id }">Delete</a>
				</td>
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>