<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Student and Teacher WebApp</title>

<!--  <link rel="stylesheet" type="text/css" href="style.css">-->
<style type="text/css">

body {
	margin-left:40px;
}
h3 {
	margin-left:40px;
}
a {
	margin-left:40px;
}

</style>


</head>

<body>
 <h1>Student and teacher WebApp, Administrator Page</h1>
 <p>This site is under development</p>
 <p>Developer: Urmas Öövel</p>
 <br>
 <h2>TraineeLand</h2>
 <h3>
    
     <a href="subjects">Subjects</a>
     
     <a href="packagetypes">Package Types</a>     
     
     <a href="criterias">Criterias</a>
     
     <a href="ecriterias">Ecriterias</a>
    

 </h3>
 <br>
 <h2>Teachers</h2>
 <h3>
 	 <a href="teachers">Teachers</a> 
    
     <a href="studypackages">Study Packages</a>
     
     <a href="ratings">Ratings of Students</a>
 
 </h3>
 <br>
 <h2>Students</h2>
 <h3>
     
     <a href="students">Students</a>
     
     <a href="studentpacks">Registered Students</a>
     
     <a href="properties">Properties of Students</a>
 
 </h3>
 
 <a href="/ContactManager02">Student only</a>
 
</body>
</html>