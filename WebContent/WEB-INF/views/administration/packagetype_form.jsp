<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>New/Edit PackageType</title>
</head>
<body>
	<div align="center">
		<h1>New/Edit PackageType</h1>
		<form:form action="savepackagetype" method="post" modelAttribute="packagetype">
		<table cellpadding="$">
			<form:hidden path="id"/>
			<tr>
				<td>Package Type Name:</td>
				<td><form:input path="packname"/></td>
			</tr>

			<tr>
				<td colspan="2" align="center"><input type="submit" value="SavePackageType"/></td>
			
			</tr>
		</table>		
		</form:form>
	
	</div>

</body>

</html>