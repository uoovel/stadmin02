<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>New/Edit StudyPackage</title>
</head>
<body>
	<div align="center">
		<h1>New/Edit StudyPackage</h1>
		<form:form action="savestudypackage" method="post" modelAttribute="studypackage">
		<table cellpadding="$">
			<form:hidden path="id"/>
			<tr>
				<td>SubjectId:</td>
				<td>
					<form:select path="subjectid">						
						<form:option value="-" label="--Please Select"/>
						<form:options items="${listSubject}" 
						itemValue="id" itemLabel="subname"/>	
					</form:select>
				</td>
			</tr>
			<tr>
				<td>PackageTypeId:</td>
				<td>
					<form:select path="packagetypeid">						
						<form:option value="-" label="--Please Select"/>
						<form:options items="${listPackagetype}" 
						itemValue="id" itemLabel="packname"/>	
					</form:select>
				</td>				
			</tr>
			<tr>
				<td>TeacherId:</td>
				<td>
					<form:select path="teacherid">						
						<form:option value="-" label="--Please Select"/>
						<form:options items="${listTeacher}" 
						itemValue="id" itemLabel="name"/>	
					</form:select>				
				<td/>
			</tr>
			<tr>
				<td>Price:</td>
				<td><form:input path="price"/></td>
			</tr>
			<tr>
				<td>Available:</td>
				<td><form:input path="available"/></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" value="SaveStudyPackage"/></td>
			
			</tr>
		</table>		
		</form:form>
	
	</div>

</body>
</html>