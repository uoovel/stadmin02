<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Subject Manager Home</title>
</head>
<body>
	<div align="center">
		<a href="index">Home</a>
		<h1>Subject List</h1>
		<h3><a href="newsubject">New Subject</a> </h3>
		<table border="1" cellpadding="5">
			<tr>
				<th>No</th>
				<th>Subject Name</th>
			</tr>
			<c:forEach items="${listSubject}" var="subject" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				<td>${subject.subname}</td>
				<td>
					<a href="editsubject?id=${subject.id }">Edit</a>
					&nbsp;&nbsp;
					<a href="deletesubject?id=${subject.id }">Delete</a>
				</td>
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>