<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>New/Edit Subject</title>
</head>
<body>
	<div align="center">
		<h1>New/Edit Subject</h1>
		<form:form action="savesubject" method="post" modelAttribute="subject">
		<table cellpadding="$">
			<form:hidden path="id"/>
			<tr>
				<td>Subject Name:</td>
				<td><form:input path="subname"/></td>
			</tr>

			<tr>
				<td colspan="2" align="center"><input type="submit" value="SaveSubject"/></td>
			
			</tr>
		</table>		
		</form:form>
	
	</div>

</body>

</html>