<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>New/Edit StudentPack</title>
</head>
<body>
	<div align="center">
		<h1>New/Edit StudentPack</h1>
		<form:form action="savestudentpack" method="post" modelAttribute="studentpack">
		<table cellpadding="$">
			<form:hidden path="id"/>
			<tr>
				<td>StudyPackageId:</td>
				<td>
					<form:select path="studypackageid">						
						<form:option value="-" label="--Please Select"/>
						<form:options items="${listStudyPackageView}" 
						itemValue="id" itemLabel="studypackagename"/>	
					</form:select>
				</td>
			</tr>

			<tr>
				<td>StudentId:</td>
				<td>
					<form:select path="studentid">						
						<form:option value="-" label="--Please Select"/>
						<form:options items="${listStudent}" 
						itemValue="id" itemLabel="studname"/>	
					</form:select>				
				<td/>
			</tr>

			<tr>
				<td>Begin:</td>
				<td><form:input path="begin"/></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" value="SaveStudentPack"/></td>
			
			</tr>
		</table>		
		</form:form>
	
	</div>

</body>
</html>