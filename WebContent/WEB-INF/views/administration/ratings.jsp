<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ratings Home</title>
</head>
<body>
	<div align="center">
		<a href="index">Home</a>
		<h1>Ratings List</h1>
		<h3><a href="newrating">New Rating</a> </h3>
		<table border="1" cellpadding="5">
			<tr>
				<th>No</th>
				<th>Student</th>
				<th>Ecriteria</th>
				<th>Score</th>

				<th>Action</th>
			</tr>
			<c:forEach items="${listRatingView}" var="ratingview" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				
				<td>${ratingview.studname}</td>
				<td>${ratingview.ecritname}</td>
				<td>${ratingview.score}</td>

				<td>
					<a href="editrating?id=${ratingview.id }">Edit</a>
					&nbsp;&nbsp;
					<a href="deleterating?id=${ratingview.id }">Delete</a>
				</td>
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>