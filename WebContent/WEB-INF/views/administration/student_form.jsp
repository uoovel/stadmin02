<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>New/Edit Student</title>
</head>
<body>
	<div align="center">
		<h1>New/Edit Student</h1>
		<form:form action="savestudent" method="post" modelAttribute="student">
		<table cellpadding="$">
			<form:hidden path="id"/>
			<tr>
				<td>Student Name:</td>
				<td><form:input path="studname"/></td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><form:input path="email"/></td>
			</tr>

			<tr>
				<td colspan="2" align="center"><input type="submit"
				value="Save"/></td>
			
			</tr>
		</table>		
		</form:form>
	
	</div>

</body>
</html>