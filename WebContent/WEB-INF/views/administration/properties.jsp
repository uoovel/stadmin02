<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Properties Home</title>
</head>
<body>
	<div align="center">
		<a href="index">Home</a>
		<h1>Properties List</h1>
		<h3><a href="newproperty">New Property</a> </h3>
		<table border="1" cellpadding="5">
			<tr>
				<th>No</th>
				<th>Student</th>
				<th>Criteria</th>
				<th>Description</th>

				<th>Action</th>
			</tr>
			<c:forEach items="${listPropertyView}" var="propertyview" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				
				<td>${propertyview.studname}</td>
				<td>${propertyview.critname}</td>
				<td>${propertyview.description}</td>

				<td>
					<a href="editproperty?id=${propertyview.id }">Edit</a>
					&nbsp;&nbsp;
					<a href="deleteproperty?id=${propertyview.id }">Delete</a>
				</td>
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>