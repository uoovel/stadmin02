<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Students entrance</title>
</head>
<body>
	<h1>For the students who want to apply for personal teacher.</h1>

    <div align="center">
        <h1>${title}</h1>
        <h2>${message}</h2>
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <h2>Welcome : ${pageContext.request.userPrincipal.name} |
                <a href="/<c:url value="/logout" />" > Logout</a></h2> 
    </c:if>              
    </div>

</body>
</html>